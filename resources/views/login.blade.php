<!DOCTYPE html>
<html >
<head>
  <meta charset="UTF-8">
<link href='https://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css">

  <link href="{{ asset('css/login-style.css') }}" rel="stylesheet">
  
</head>

<body style="background: transparent url('/images/bg1.jpg') repeat;">
<div class="panel">
  <ul class="panel__menu" id="menu">
    <hr/>
    <li id="signIn"> <a href="#">Login</a></li>
    <li id="signUp"><a href="#">Sign up</a></li>
  </ul>
  <div class="panel__wrap">
    <div class="panel__box active" id="signInBox">
      <label>Email
        <input type="email"/>
      </label>
      <label>Password
        <input type="password"/>
      </label>
        <input type="submit" value="Allow Me!"/>
    </div>
    <div class="panel__box" id="signUpBox">
      <label>Email
        <input type="email"/>
      </label>
      <label>Password
        <input type="password"/>
      </label>
      <label>Confirm password
        <input type="password"/>
      </label>
        <input type="submit" value="Add Me!"/>
    </div>
  </div>
</div>
  
  <form class="form-horizontal" role="form" method="POST" action="{{ url('/auth/login') }}">
<input type="email" class="form-control" name="email" value="{{ old('email') }}">
<input type="password" class="form-control" name="password">
<button type="submit" class="btn btn-primary">Login </button>
</form>
   <script src="{{asset('js/login-index.js')}}"></script>

</body></html>