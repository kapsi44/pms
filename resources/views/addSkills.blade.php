@extends('layout')
@section('content')
<style>
    .split-list ul{
        list-style-type: none;
        float: left;
    }
    .split-list li{
        padding-right: 2em;
        line-height: 1.5em;
    }
    #add_skill{
        margin-left: 24em;
        margin-bottom: .5em;
    }
    .inner{
        background: white;
    }
    .skills{
        display: inline-flex;
        padding-top: 1em;
        font-size: 1.5em;
    }
    .user_head{
        font-size: 2em;
        text-transform: capitalize;
    }
    #delUser,#editUser{
        float: right;
        margin-right: 5px;
    }
    .note{
        font-size:.9em;
        width:21%
    }
</style>

<section id="about section-padding" style="padding-right:0%">
    <div class="site-wrapper">
        <div class="site-wrapper-inner">
            <div class="cover-container">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <span class="glyphicon glyphicon-user" style="margin-right:10px">
                            <span class="user_head"> {{$user['name']}} Skills</span> <br></span>
                        @if((Auth::user()->designation_id) == 1)
                        <button  id="delUser" class="btn btn-danger">Delete User</button>
                        <button onclick="location.href = '/edit/user/{{$user['id']}}';" id="editUser" class="btn btn-info" >Edit</button>                                                        
                        @endif
                    </div>            
                    <div class="inner cover">
                        {!! Form::open(array('action' => array("ProfileController@addSkills",$user['id']),'onsubmit' => "return confirm('Are you sure you want to add these Skills?')", )) !!} 
                        <div class="skills">
                            <div class="known_skills">
                                <ul class="split-list">
                                    @foreach($skills as $skill)
                                    @if(isset($user_skills) && in_array($skill['id'],$user_skills))

                                    <li id="user_skill{{$skill['id']}}">
                                         @if((Auth::user()->designation_id) == 1)
                                         <input type="button" data-user-id="{{$user['id']}}" data-skill-id="{{$skill['id']}}" class="deleteSkill btn btn-sm btn-danger" value="X"/>
                                         @else
                                         <input tabindex="1" type="checkbox" name="skill[{{$user['id']}}][{{$skill['id']}}]" id="{{$skill['id']}}" checked="true" disabled="true">
                                         @endif
                                        <span style="color:green">{!! Form::label($skill['skills']) !!} 
                                        </span> 
                                    </li>   
                                    @endif
                                    @endforeach
                                </ul>
                            </div>
                            <div style="display: block;" class="unknown-skills"> 
                                <ul class="split-list">
                                    @foreach($skills as $skill)
                                    @if(!in_array($skill['id'],$user_skills))
                                    <li>  
                                        <input tabindex="1" type="checkbox" name="skill[{{$user['id']}}][{{$skill['id']}}]" id="{{$skill['id']}}">
                                        <span style="color:rgb(255, 129, 0)">{!! Form::label($skill['skills']) !!} </span> 
                                    </li>
                                    @endif
                                    @endforeach
                                </ul>
                            </div>
                        </div>    
                        {!! Form::submit('Add Skills!',array('class'=>'btn btn-lg btn-success','id'=> 'add_skill')) !!}
                        <div class="alert col-lg-4 alert-info note" style="float: right">
                            <strong>Note!</strong> Skill in <span style="background-color: green;color:white">Green</span> are User known.
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
</section>
<script src="http://code.jquery.com/jquery-2.0.3.min.js" data-semver="2.0.3" data-require="jquery"></script>
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js" data-semver="3.1.1" data-require="bootstrap"></script>
<script src="http://bootboxjs.com/bootbox.js"></script>
<script>
    $(function ($) {
        var num_cols = 3,
                container = $('.split-list'),
                listItem = 'li',
                listClass = 'sub-list';
        container.each(function () {
            var items_per_col = new Array(),
                    items = $(this).find(listItem),
                    min_items_per_col = Math.floor(items.length / num_cols),
                    difference = items.length - (min_items_per_col * num_cols);
            for (var i = 0; i < num_cols; i++) {
                if (i < difference) {
                    items_per_col[i] = min_items_per_col + 1;
                } else {
                    items_per_col[i] = min_items_per_col;
                }
            }
            for (var i = 0; i < num_cols; i++) {
                $(this).append($('<ul ></ul>').addClass(listClass));
                for (var j = 0; j < items_per_col[i]; j++) {
                    var pointer = 0;
                    for (var k = 0; k < i; k++) {
                        pointer += items_per_col[k];
                    }
                    $(this).find('.' + listClass).last().append(items[j + pointer]);
                }
            }
        });
    });

    $('.deleteSkill').click(function () {
        var uid = $(this).data('user-id');
        var sid = $(this).data('skill-id');

        var dialog = bootbox.dialog({
            title: 'Delete Skills',
            message: "Are you sure want to delete?",
            onEscape: true,
            buttons: {
                cancel: {
                    label: 'Cancel',
                    className: 'btn-default',
                    callback: function () {
                    }
                },
                confirm: {
                    label: 'OK',
                    className: 'btn-primary',
                    callback: function () {
                        $.ajax({
                            method: 'GET',
                            url: '/deleteSkill/' + uid + '/' + sid,
                            success: function (response) {
                                $('#user_skill' + sid).remove();
                            }
                        });
                    }
                }
            }
        });

    });
</script>
@endsection