@extends('layout')

@section('content')
<style>
    .container{
        background: none;
    }
    .addSkill{
        margin-top: 2em;
        margin-left: 5em;
    }
    .jrequired{
        width: 100%
    }
    </style>
    <div class="site-wrapper-add-user">
    <div class="site-wrapper-inner">
        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        <div class="container">
            <div class="row">
                <div class="col-md-6" style="margin-left:250px;margin-top:50px;">
                    <div class="panel panel-primary" style="background-color: white;">
                        <div class="panel-heading">
                            <h3 class="panel-title"><span class="glyphicon glyphicon-user" style="margin-right:10px"></span>Add New Skill</h3>
                        </div>
      {!! Form::open(array('action' => "ProfileController@saveSkills",'class'=>'form-horizontal','id'=>'surveyForm', 'method'=>'post')) !!} 
                        <form id="surveyForm" method="post" class="form-horizontal">
                        <div class="form-group" style="margin-top: 3em;">
                            <label class="col-xs-3 control-label">Skills    </label>
        <div class="col-xs-5">
            <input type="text" class="form-control" name="skills[]" placeholder="Ex: PHP"/>
        </div>
                            <div class="col-xs-4">
                                <button type="button" class="btn btn-default addButton"><i class="fa fa-plus"></i></button>
                            </div>
                        </div>

                        <!-- The option field template containing an option field and a Remove button -->
                        <div class="form-group hide" id="optionTemplate">
                            <div class="col-xs-offset-3 col-xs-5">
                               
                                {!! Form::text('skills[]', Input::old('name'),array('class'=>'form-control','placeholder'=>'Ex: PHP', 'required')) !!}
                                
                            </div>
                            <div class="col-xs-4">
                                <button type="button" class="btn btn-default removeButton"><i class="fa fa-minus"></i></button>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-xs-5 col-xs-offset-3">

                                <button type="submit" class="btn btn-default">Add Them!!</button>
                            </div>
                        </div>
                    </form>
                        </div>                       
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="http://formvalidation.io/vendor/formvalidation/js/formValidation.min.js"></script>
<script src="http://formvalidation.io/vendor/formvalidation/js/framework/bootstrap.min.js"></script>
<script>
$(document).ready(function() {
    // The maximum number of options
    var MAX_OPTIONS = 10;

    $('#surveyForm')
        .formValidation({
            framework: 'bootstrap',
            icon: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                question: {
                    validators: {
                        notEmpty: {
                            message: 'The question required and cannot be empty'
                        }
                    }
                },
                'skills[]': {
                    validators: {
                        notEmpty: {
                            message: 'The option required and cannot be empty'
                        },
                        stringLength: {
                            max: 100,
                            message: 'The option must be less than 100 characters long'                           
                        }
                    }
                }
            }
        })

        // Add button click handler
        .on('click', '.addButton', function() {
            var $template = $('#optionTemplate'),
                $clone    = $template
                                .clone()
                                .removeClass('hide')
                                .removeAttr('id')
                                .insertBefore($template),
                $option   = $clone.find('[name="skills[]"]');

            // Add new field
            $('#surveyForm').formValidation('addField', $option);
        })

        // Remove button click handler
        .on('click', '.removeButton', function() {
            var $row    = $(this).parents('.form-group'),
                $option = $row.find('[name="skills[]"]');

            // Remove element containing the option
            $row.remove();

            // Remove field
            $('#surveyForm').formValidation('removeField', $option);
        })

        // Called after adding new field
        .on('added.field.fv', function(e, data) {
            // data.field   --> The field name
            // data.element --> The new field element
            // data.options --> The new field options

            if (data.field === 'skills[]') {
                if ($('#surveyForm').find(':visible[name="skills[]"]').length >= MAX_OPTIONS) {
                    $('#surveyForm').find('.addButton').attr('disabled', 'disabled');
                }
            }
        })

        // Called after removing the field
        .on('removed.field.fv', function(e, data) {
           if (data.field === 'skills[]') {
                if ($('#surveyForm').find(':visible[name="skills[]"]').length < MAX_OPTIONS) {
                    $('#surveyForm').find('.addButton').removeAttr('disabled');
                }
            }
        });
});
</script>    
@endsection