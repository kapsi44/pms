@extends('layout')

@section('content')
<style>
    .split-list ul{
        list-style-type: none;
        float: left;
        font-size: 1.3em;
        margin-left: 3em;
        padding-bottom: 2em
    }
    .split-list li{
        padding-right: 2em;
        line-height: 1.5em;
    }
    .split-list {
        padding-bottom: 9em;
    }
    .entypo-check{
        color:#36ac3e;
    }
    .entypo-cancel{
        color:#d33536;
    }
    .md-close{
        color:black;
    }
    .dataTables_filter, #example_length, #example_info, #example_paginate { visibility: hidden;}
    #pakete button {
        border: none;
        border-radius: 5px;
        background: #D43536;
        padding: 10px 25px;
        color: #fff;
        transition: box-shadow 0.1s, border-radius 0.3s;
        font: 300 1.2rem 'Open Sans', sans-serif;
        cursor: pointer;
    }
    .sorting, .sorting_asc{text-align: center;}
    #modal-11{opacity:.96;}
    .md-content{background: hsla(208, 56%, 46%, 0.94)}
    #msg{margin-left: 6em;width:80% }
    .users,.skill_set{text-transform: capitalize}
    .title1{
        color:white;
        font-size: 4em;
    }
    .pointer{
        top: 4px;
        padding-right: 7px; 
    }
    .panel-title,#skill_id{
        font-size:1.5em;
    }
    .amcharts-main-div a {
        display: none !important;
    }
    
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>		
<script src="{{asset('js/owl.carousel.js')}}"></script>		
<script src="{{asset('js/jquery.sticky.js')}}"></script>		
<script src="{{asset('js/main.js')}}"></script>		

<!-- END of section-wrap -->
<section id="pakete">
    <div class="smooth-scroll">
       @if((Auth::user()->designation_id) == 1)
        <h1 class="title1">Skill Profiles of the Users</h1> 
       @else
        <h1 class="title1"> Your Skills Profile</h1>
       @endif 
    </div>
    @if (Session::has('message'))
    <div class="alert alert-success" id="msg">{{ Session::get('message') }}</div>
    @endif
    @if((Auth::user()->designation_id) == 1)
    <div class="col-md-12 wow slideInRight">   
        <div class="container-fluid">    
            <br>
            <br>
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <span class="glyphicon glyphicon-circle-arrow-right pointer"></span>Team Members With Different skill sets are listed here!!</h3>
                </div>
                <div class="panel-body two-col">
                    <div class="row">
                        <div class="panel-footer" style="max-width:1000px;background:none;">
                            <div class="tabbable-panel">
                                <div class="tabbable-line">
                                    <div style="margin-left: 0%;">
                                        <ul class="split-list skill_set" id="checkboxlist">
                                            <li><input type="checkbox" id="selectall" checked="checked" class="case "><b style="color:green;">Select All</b></li>
                                            @foreach ($skills as $skill)
                                                @if(isset($skill_count[$skill->id]))
                                                    <li> <input type="checkbox"  name="<?php $rep=array(' ','.'); echo str_replace($rep,'',$skill['skills']);?>" id="<?php $rep=array(' ','.'); echo str_replace($rep,'',$skill['skills']);?>" class="case chk skill_class" checked="checked" value="{{$skill['skills']}}"> <label for="{{$skill['skills']}}"> {{$skill['skills']}}(<b>{{$skill_count[$skill->id]['order_count']}}</b>) </label> </li>
                                                @else 
                                                    <li> <input type="checkbox"  name="<?php $rep=array(' ','.'); echo str_replace($rep,'',$skill['skills']);?>" id="<?php $rep=array(' ','.'); echo str_replace($rep,'',$skill['skills']);?>" class="case chk skill_class" checked="checked" value="{{$skill['skills']}}"> <label for="{{$skill['skills']}}"> {{$skill['skills']}}(<b>0</b>) </label> </li>
                                                @endif
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>          
                        </div>
                    </div>
                </div>
            </div>
            <div class="front" id="parent checkboxlist">
                <table border="0" class="table" id="fixTable">
                    <thead class="table-inverse">
                        <tr>
                            <th style="background-color: #337ab7;" colspan="3">
                                <span style="float:left">Sno</span> Developers
                            </th>
                            
                            @foreach ($skills as $skill)

                            <?php
                            $skill_name = $skill->skills;
                            $skill_ids[] = $skill->id;
                            $skill_id = $skill->id;
                            ?>
                            <th id='{{$skill_id}}' class="<?php $rep=array(' ','.'); echo str_replace($rep,'',$skill['skills']);?> techAll" style="font-weight: 900 !important">
                                {{$skill_name}} 
                            </th> 
                            @endforeach
                        </tr>

                    </thead>
                    <tbody id="myRow">
                        <?php $counter = 1; ?>
                        @foreach ($users as $user)
                    <div id="myModal" class="modal">
                        <div class="modal-content">
                            <span class="close btn-danger">×</span>
                            <h2>{{ $user }}</h2>
                        </div>
                    </div> 
                    <tr>
                        <th scope="row" colspan="3">
                            <span style="float:left;padding-right: 14em">{{$counter}}.</span> <?php $counter++; ?>
                            <a class="font-color users"  href="/addSkills/{{$user['id']}}" >
                                <b> {{ substr($user['name'],0,20) }}</b>
                            </a>    
                        </th>
                        
                        @foreach ($skills as $skill) 
                        <td class="<?php $rep=array(' ','.'); echo str_replace($rep,'',$skill['skills']);?> techAll">
                            @if(isset($ids[$user['id']]) && in_array($skill->id, $ids[$user['id']]))
                            <span class='entypo-check'></span>
                            @else 
                            <span class='entypo-cancel'></span>
                            @endif
                        </td>    
                        @endforeach
                    </tr>
                    @endforeach         
                    </tbody>
                    <tr>
                        <th scope="row" colspan="3"> Total </th>
                        @foreach ($skills as $skill)
                            @if(isset($skill_count[$skill->id]))
                                <td class="<?php $rep=array(' ','.'); echo str_replace($rep,'',$skill['skills']);?> techAll skill_id" data-skills-id="{{$skill['skills']}}"> <b>{{$skill_count[$skill->id]['order_count']}}</b> </td>
                            @else
                                <td class="<?php $rep=array(' ','.'); echo str_replace($rep,'',$skill['skills']);?> techAll"><b>0</b></td>
                            @endif    
                        @endforeach
                    </tr>
                    <tr>
                        <td colspan="3"> </td>   
                        @foreach ($skills as $skill)
                        <td class="<?php $rep=array(' ','.'); echo str_replace($rep,'',$skill['skills']);?> techAll"><button class="md-trigger md-setperspective details" id="{{$skill['id']}}" data-modal="modal-11">Details</button></td>
                        @endforeach 
                    </tr>
                </table>   
            </div>
        </div>
        <button  class="paginate alert-success" style="color: white;background-color: black;border-color: #d6e9c6;margin-left: 32em " id="previous" value="Previous">Previous</button> 
        <button  class="paginate btn-default alert-success" style="color: white;background-color:black;border-color: #d6e9c6;margin-left: 10em" id="next" value="Next">Next</button>
        <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <span class="glyphicon glyphicon-circle-arrow-right pointer"></span>User Skills Statistics!!
                        <b style="float:right"><span class="glyphicon glyphicon-circle-arrow-right pointer"></span>Total no. of Users: {{$user_count}}</b>
                    </h3>
                </div>
                <div class="panel-body two-col">
                    <div class="row">
                        <div class="panel-footer" style="max-width:1000px;background:none;">
                            <div class="tabbable-panel">
                                <div class="tabbable-line">
                                    <div style="margin-left: 0%;">
                                        <div id="chartdiv" style="width: 100%; height: 400px;"></div>
                                    </div>
                                </div>
                            </div>          
                        </div>
                    </div>
                </div>
            </div>
        @else
        <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <span class="glyphicon glyphicon-circle-arrow-right pointer"></span>Your updated skill sets are listed here!!</h3>
                </div>
                <div class="panel-body two-col">
                    <div class="row">
                        <div class="panel-footer" style="max-width:1000px;background:none;">
                            <div class="tabbable-panel">
                                <div class="tabbable-line">
                                    <div style="margin-left: 0%;">
                                        <ul class="split-list" id="skill_id" data-field-id="{{Auth::user()->id}}">

                                        </ul>
                                    </div>
                                    <div class="tab-content">
                                    </div>
                                </div>
                            </div>          
                        </div>
                    </div>
                </div>
            </div>
        @endif
        
</section><!-- END of section-wrap -->
<div class="md-modal md-effect-11" id="modal-11" style="font-size: 1em">
    <div class="md-content">
        <h3>Users </h3>
        <div>
            <div class="alert alert-success" id="avail_resources"></div>
            <table id="example" class="display" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Project/Bench</th>
                    </tr>
                </thead>    
                <tbody id="content" style="color: black;text-align: center;">
                </tbody> 
            </table>
            <button class="md-close btn-danger">Close me!</button>
        </div>
    </div>
</div>

<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>

<script src="{{asset('/js/index.js')}}"></script>
<script src="{{asset('/js/classie.js')}}"></script>
<script src="{{asset('/js/modalEffects.js')}}"></script>
<script src="{{asset('/js/cssParser.js')}}"></script>

<script>
//chart starts
var chart;
var legend;
var skill_name = [];
var skill_count = [];
var chartData = [];
$(".skill_id").each(function () {
    var skill = $(this).attr("data-skills-id");
    chartData.push({value1: skill, value2: $(this).text()});
});   

AmCharts.ready(function () {
    chart = new AmCharts.AmPieChart();
    chart.dataProvider = chartData;
    chart.titleField = "value1";
    chart.valueField = "value2";
    chart.outlineColor = "#FFFFFF";
    chart.outlineAlpha = 0.8;
    chart.outlineThickness = 2;
    chart.color = "black";
    chart.fontSize = "15px";
    chart.balloonText = "[[title]]<br><span style='font-size:14px'><b>[[value2]]</b> ([[percents]]%)</span>";
    chart.depth3D = 15;
    chart.angle = 30;

    chart.write("chartdiv");
});
//chart ends

$('.details').click(function () {
    var uid = this.id;
    $.ajax({
        method: 'GET', 
        url: '/detail/' + uid, 
        success: function (response) {
            var content = [];
            var count = 0;
            for (var prop in response) {
                var id = response[prop]['id'];
                var name = response[prop]['name'];
                var project = response[prop]['project_id'] > 1 ? "<b><span style=" + "color:red" + ">Project</span></b>" : "<b><span style=" + "color:green" + ">Bench</span></b>";
                content[prop] = "<tr><td>" + id + "</td><td>" + name + "</td><td>" + project + "</td></tr>";
                if (response[prop]['project_id'] <= 1) {
                    count++;
                }
            }
            $("#avail_resources").html("Total Available = " + count);
            $("#content").html(content);
        },
        error: function (textStatus, errorThrown) { 
            console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
        }
    });
});

$(document).ready(function () {
    $('#example').DataTable({
        "scrollY": "200px",
        "scrollCollapse": true,
        "paging": false,
        "searching": false,
        "bInfo": false
    });
});    

$(document).ready(function () {
    var uid = $('#skill_id').data('field-id');
    $.ajax({
        method: 'GET', 
        url: '/getUserSkills/' + uid, 
        success: function (response) {
            var content = [];
            for(var skill in response){
                var skills = response[skill]['skills'];
                content[skill] = "<li>" + skills + "</li>";
            }
            $('#skill_id').html(content);
        },
         error: function (textStatus, errorThrown) { 
            console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
        }
    });

});



setTimeout(function () {
    $("#msg").fadeOut().empty();
}, 5000);
$(function ($) {
    var num_cols = 3,
            container = $('.split-list'),
            listItem = 'li',
            listClass = 'sub-list';
    container.each(function () {
        var items_per_col = new Array(),
                items = $(this).find(listItem),
                min_items_per_col = Math.floor(items.length / num_cols),
                difference = items.length - (min_items_per_col * num_cols);
        for (var i = 0; i < num_cols; i++) {
            if (i < difference) {
                items_per_col[i] = min_items_per_col + 1;
            } else {
                items_per_col[i] = min_items_per_col;
            }
        }
        for (var i = 0; i < num_cols; i++) {
            $(this).append($('<ul ></ul>').addClass(listClass));
            for (var j = 0; j < items_per_col[i]; j++) {
                var pointer = 0;
                for (var k = 0; k < i; k++) {
                    pointer += items_per_col[k];
                }
                $(this).find('.' + listClass).last().append(items[j + pointer]);
            }
        }
    });
    
var firstRecord = 0;
var pageSize = 5;
var tableRows = $("#myRow > tr");

$(".paginate").click(function (e) {
    e.preventDefault();
    var tmpRec = firstRecord;
    if ($(this).attr("id") == "next") {
        tmpRec += pageSize;
    } else {
        tmpRec -= pageSize;
    }
    if (tmpRec < 0 || tmpRec > tableRows.length) return
    firstRecord = tmpRec;
    paginate(firstRecord, pageSize);
});

var paginate = function (start, size) {
    var end = start + size;
    tableRows.hide();
    tableRows.slice(start, end).show();
}


paginate(firstRecord, pageSize);
});
</script>
<script src="//code.jquery.com/jquery-1.12.4.js"></script>   
<script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>   
<script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap4.min.js"></script>
<script src="{{asset('js/wow.min.js')}}"></script>
<script src="{{asset('js/owl.carousel.js')}}"></script>
<script src="{{asset('js/jquery.sticky.js')}}"></script>
<script src="{{asset('js/tableHeadFixer.js')}}"></script>
<script src="{{asset('js/prefixfree.min.js')}}"></script>
@endsection
