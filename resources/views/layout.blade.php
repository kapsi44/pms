<!DOCTYPE html>
<html >
    <head>
        <meta charset="UTF-8">
        <title>Users Profile</title>

        <link href="{{ asset('/css/layout.css') }}" rel="stylesheet">
        <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}">
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css'>
        <link href="https://fonts.googleapis.com/css?family=Caveat+Brush" rel="stylesheet">        
        <link href="{{ asset('css/font-awesome.css') }}" rel="stylesheet">
        <link rel="stylesheet" href="{{ asset('css/animate.css') }}">
        <link rel="stylesheet" href="{{ asset('css/owl.theme.css') }}">
        <link rel="stylesheet" href="{{ asset('css/owl.carousel.css') }}">
        <link href="{{ asset('css/home.css') }}" rel="stylesheet">
        <link href="{{ asset('css/responsive.css') }}" rel="stylesheet">
        <script src="{{asset('/js/prefixfree.min.js')}}"></script>
        <link rel="stylesheet" type="text/css" href="{{ asset('/css/component.css') }}"/>
        <link rel="stylesheet" type="text/css" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"/>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.13/css/dataTables.jqueryui.min.css"/>
        <link rel="stylesheet" href="{{ asset('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css') }}">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <!-- Optional theme -->
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.6/angular.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="{{ asset('/css/piechart.css') }}"/>
        <script src="{{asset('/js/amcharts.js')}}"></script>
        <script src="{{asset('/js/pie.js')}}"></script>
        <style>
            .container{
                background: none;
            }
            body{
                color: #333333;
                font-family:  'Caveat Brush', cursive;
            }
            .name-header{
                margin-left: 15em;
                font-size: 1.5em;
                position: relative;
                top: 1em;
            }
            #back-to-top {
                position: fixed;
                bottom: 40px;
                right: 40px;
                z-index: 9999;
                width: 32px;
                height: 32px;
                text-align: center;
                line-height: 30px;
                background: #f5f5f5;
                color: #444;
                cursor: pointer;
                border: 0;
                border-radius: 2px;
                text-decoration: none;
                transition: opacity 0.2s ease-out;
                opacity: 0;
                }
            #back-to-top:hover {
                background: #e9ebec;
            }
            #back-to-top.show {
                opacity: 1;
            }
            #content {
                height: 2000px;
            }
        </style>
    </head>
    <body style="background: transparent url('/images/bg1.jpg') repeat;" ng-controller="Ctrl">
        <nav class="navbar navbar-default navbar-static-top">
        <div id="sticky-wrapper" class="container sticky-wrapper">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    <li><a href="{{ url('/home') }}">Home</a></li>
                </ul>
                <span class="name-header">Hi {{ Auth::user()->name }}!!</span>
                        <ul class="nav navbar-nav navbar-right">
                            @if((Auth::user()->designation_id) == 1)
                            <li><a href="/updateSkills" class="smooth-scroll">Add Skills</a></li>
                            <li><a href="/addUser" class="smooth-scroll">Recruit Developer</a></li>
                            @endif
                            <li><a href="/addSkills/{{Auth::user()->id}}" class="smooth-scroll">Update Skills</a></li>
                            <li><a href="/logout" class="smooth-scroll">Logout</a></li>
                        </ul>
                    </div>
                </div>
            </nav>
            </div>       
        </header><!--/#navigation-->
        <section id="about">
            <div class="container">

                @yield('content')
                <a href="#" id="back-to-top" title="Back to top"><span class="glyphicon glyphicon-chevron-up btn btn-success"></span></a>
            </div>
        </section>
        <script>
            //scroll top
            if ($('#back-to-top').length) {
                var scrollTrigger = 100, // px
                    backToTop = function () {
                        var scrollTop = $(window).scrollTop();
                        if (scrollTop > scrollTrigger) {
                            $('#back-to-top').addClass('show');
                        } else {
                            $('#back-to-top').removeClass('show');
                        }
                    };
                backToTop();
                $(window).on('scroll', function () {
                    backToTop();
                });
                $('#back-to-top').on('click', function (e) {
                    e.preventDefault();
                    $('html,body').animate({
                        scrollTop: 0
                    }, 700);
                });
            }
        </script>
    </body>
    <div class="alert col-lg-4 alert-info note" style="float: right">
        <p style="font-size: 12px;font-weight:bold">For any queries, Mail to: <span style="color:#407d9c;">sundaravel.somasundaram@aspiresys.com</span></p>
    </div>
</html>    