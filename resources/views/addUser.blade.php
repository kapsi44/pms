@extends('layout')

@section('content')
<style>
    .container{
        background: none;
    }
    .addSkill{
        margin-top: 2em;
        margin-left: 5em;
    }
    .jrequired{
        width: 100%
    }
    </style>
    <div class="site-wrapper-add-user">
    <div class="site-wrapper-inner">
        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        <div class="container">
            <div class="row">
                <div class="col-md-6" style="margin-left:250px;margin-top:50px;">
                    <div class="panel panel-primary" style="background-color: white;">
                        <div class="panel-heading">
                            <h3 class="panel-title"><span class="glyphicon glyphicon-user" style="margin-right:10px"></span>Add New User</h3>
                        </div>
                        @if(isset($user))
                        {!! Form::model($user, ['action' => ['ProfileController@editUser', $user->id], 'method' => 'patch']) !!}
                        @else
                        {!! Form::open(array('action' => 'ProfileController@save','class'=>'form-inline','id'=>'addUser')) !!} 
                        @endif
                        <div class="jumbotron" style="background-color:white">
                            <ul style="list-style-type: none;" class="form-group">
                                 @if(isset($user))
                                 <li>
                                    {!! Form::text('name', Input::old('name'),array('class'=>'jrequired jname','placeholder'=>'Name', 'disabled')) !!}
                                </li><br>
                                <li>        
                                    {!! Form::text('email', Input::old('email'), array('class'=>'jrequired jemail','placeholder'=>'Email', 'disabled')) !!}
                                </li><br>
                                @else
                                <li>
                                    {!! Form::text('name', Input::old('name'),array('class'=>'jrequired jname','placeholder'=>'Name', 'required')) !!}
                                </li><br>
                                <li>        
                                    {!! Form::text('email', Input::old('email'), array('class'=>'jrequired jemail','placeholder'=>'Email', 'required')) !!}
                                </li><br>
                                @endif
                                <li class="col-lg-10">
                                    {!! Form::Label('products', 'Project Status:') !!}
                                    {!! Form::select('project_id', $projects, Input::old('project_id'), ['class' => 'form-control']) !!}
                                </li> 
                            </ul>
                            @if(isset($user))
                                {!! Form::submit('Edit User!',array('class'=>'btn btn-lg btn-success addSkill')) !!}
                            @else
                                {!! Form::submit('Add User!',array('class'=>'btn btn-lg btn-success addSkill')) !!}
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}
<script>
    $(document).ready(function () {
        $("#addUser").validate();
        jQuery.validator.addClassRules("jrequired", {
            required: true
        });
        jQuery.validator.addClassRules("jname", {
            minlength: 2,
            digits: false
        });
        jQuery.validator.addClassRules("jemail", {
            email: true
        });
        $.validator.addMethod("required", $.validator.methods.required,
                "Please enter this field");
        $.validator.addMethod("minlength", $.validator.methods.minlength,
                "Minimum of 2 characters");
        $.validator.addMethod("email", $.validator.methods.email,
                "Enter valid email");
        $.validator.addMethod("lettersonly", $.validator.methods.lettersonly,
                "Enter valid name");

    });
</script>    
@endsection