$(document).ready( function(){

	// The flipping control-center
	// Two functions for every box :(
var arr = [];
for( var i=1 ; i<30; i++){
    arr.push(i); 
}

jQuery.each( arr, function( i, val ) {
  $('.show-me_'+val).on("click", function() {
	  $(".box").addClass('flip'+val);
	});
   $('.hide-me_'+val).on("click", function() {
	  $(".box").removeClass('flip'+val);  
	});     
});

	$('[class*="-me_"]').click( function(){
		var boxclass = $('.box').attr('class');
		$('p.boxclass').html(boxclass);
	});

	
// Table Carousel

$('button.next-table').click( function(){
	var currentone = $('.responsive-tables .active');
	var currenttwo = $('.responsive-tables .activetwo'); 
	var next = $('.responsive-tables .activetwo + [class*="little"]');
	var last = $('.responsive-tables .last');
	next.addClass('activetwo');
	currentone.removeClass('active').addClass('last');
	currenttwo.removeClass('activetwo').addClass('active');
	last.removeClass('last');

	var echonext = next.attr('class');
	//alert(echonext);
	

	if( echonext === undefined ){
		$('.little1').addClass('activetwo');
	}

});

$('button.prev-table').click( function(){
	var currentone = $('.responsive-tables .active');
	var currenttwo = $('.responsive-tables .activetwo'); 
	var prev = $('.responsive-tables .last').prev('[class*="little"]');
	var last = $('.responsive-tables .last');
	prev.addClass('last');
	currentone.removeClass('active').addClass('activetwo');	
	last.removeClass('last').addClass('active');
	currenttwo.removeClass('activetwo');

	//alert(prev.attr('class'));

	if( prev.attr('class') === undefined ){
		$('.responsive-tables .little9').addClass('last');
	}

});

$("input:checkbox:not(:checked)").each(function() {
    var column = "table ." + $(this).attr("name");
    $(column).hide();
});

$("input:checkbox").click(function(){
    var column = "table ." + $(this).attr("name");
    $(column).toggle();
});
$("#selectall").change(function () {
    if($("#selectall").is(':checked')) {
      $('input.case').prop('checked', true);
      $(".techAll").show();
    } else {
        $('input.case').prop('checked', false);
        $(".techAll").hide();
    }
});

}); // .ready-END