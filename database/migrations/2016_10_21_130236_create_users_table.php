<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::dropIfExists('users');
        Schema::create('users', function (Blueprint $table) {
                $table->increments('id');
                $table->string('email', '100')->unique();
                $table->string('password');
                $table->string('name', '30');
                $table->integer('designation_id')->unsigned();
                $table->integer('project_id')->unsigned();
                $table->integer('status_id')->unsigned();
                $table->foreign('project_id')
                    ->references('id')->on('projects')
                    ->onDelete('cascade');
                $table->rememberToken();

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::drop('users');
    }
}
