<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'desgination_id', 'project_id'];

    /**
     * The roles that belong to the user.
     */
    public function skills()
    {
        return $this->belongsToMany('App\Model\Skills');
    }
}
