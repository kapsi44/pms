<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Skills extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'skills';
        public $timestamps = false;
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['skills'];
        
         public function users()
    {
        return $this->belongsToMany('App\Users', 'user_skills', 'user_id', 'skill_id');
    }
        
}

