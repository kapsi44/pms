<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserSkills extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'user_skills';
        public $timestamps = false;

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['user_id','skill_id'];
        
        public function users()
        {
            return $this->hasMany('App\Users','id');
        }
        public function skills()
        {
            return $this->hasMany('App\Skills','id');
        }
}
