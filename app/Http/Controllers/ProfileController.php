<?php

namespace app\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use App\Users;
use App\Skills;
use App\UserSkills;
use App\Projects;

class ProfileController extends Controller
{
    /**
     * Show the application dashboard to the admin.
     *
     * @return statsTable
     */
    public function index()
    {
        $ids = array();
        $skill_data = array();
        $user_item = array();
        $users = Users::all();
        $user_count = Users::count();
        $skills = Skills::orderBy('id', 'ASC')->get();
        foreach ($users as $user) {
            $use = \App\Users::find($user['id']);
            foreach ($use->skills as $skill) {
                $skill_data[] = $skill['pivot'];
            }
        }
        $skill_count = UserSkills::select(DB::raw('count(*) as order_count, skill_id'))
                          ->groupBy('skill_id')
                          ->get()
                          ->keyBy('skill_id');
        $project_count = UserSkills::select(DB::raw('count(*) as availbleCount, skill_id'))
                           ->leftJoin('users', 'user_skills.user_id', '=', 'users.id')
                           ->where('users.project_id', '=', 0)
                           ->get();
        
        foreach ($skill_data as $userId) {
            $user_item[] = $userId['skill_id'];
        }
        for ($k = 0; $k < count($users); ++$k) {
            for ($i = 0; $i < count($skill_data); ++$i) {
                if ($users[$k]['id'] == $skill_data[$i]['user_id']) {
                    $ids[$users[$k]['id']][] = $skill_data[$i]['skill_id'];
                }
            }
        }

        return view('profileHome')->with([
        'users' => $users,
        'skills' => $skills,
        'user_skills' => $skill_data,
        'user_count' => $user_count,    
        'skill_count' => $skill_count,
        'user_skill_id' => $user_item,
        'ids' => $ids,
        'project_count' => $project_count,
    ]);
    }
    /**
     * To edit or add the skills of users.
     *
     * @return homepage
     */
    public function addSkills(Request $request, $id)
    {
        if($request->isMethod('POST') && isset($id)) {
            $this->updateSkills($id);
            
            return Redirect::to('home');
            
        } elseif ($request->isMethod('GET') && isset($id) && (Auth::user()->id == $id || Auth::user()->designation_id == 1)) {
            $skill_data = '';
            $user_skill_id = '';
            $users = Users::findOrFail($id);

            $use = Users::find($id);
            foreach ($use->skills as $skill) {
                $skill_data[] = $skill['pivot'];
            }
            if (!empty($skill_data)) {
                for ($i = 0; $i < count($skill_data);++$i) {
                    $user_skill_id[] = $skill_data[$i]['skill_id'];
                }
            } else {
                $user_skill_id[] = 0;
            }
            $skills = Skills::orderBy('id', 'ASC')->get();
            $rem = count($skills) % 10;
            $spilts = (count($skills) - $rem) / 10;

            return view('addSkills')->with([
            'user' => $users,
            'skills' => $skills,
            'user_skills' => $user_skill_id,
            'splits' => $spilts,
            ]);
        } else {
            return view('errors/503');
        }
    }
    /**
     * Updates the skills of the user.
     *
     * 
     */
    public function updateSkills($id)
    {  
        $skills = Input::get('skill');
        $skill_data = '';
        $user = Users::find($id);

        foreach ($user->skills as $skill) {
            $skill_data[] = $skill['pivot']['skill_id'];
        }
        if (empty($skill_data)) {
            $skill_data[] = 0;
        }
        if (!empty($skills[$id])) {
            foreach ($skills[$id] as $key => $value) {
                if (!in_array($key, $skill_data)) {
                    $user_skills = new UserSkills();
                    $user_skills->user_id = $id;
                    $user_skills->skill_id = $key;
                    $user_skills->save();
                }
            }
            Session::flash('message', 'User Skills were updated');
        }
    }
    /**
     * Displays the add user page
     *
     * @return addUser page
     */
    public function register()
    {   
        if(Auth::user()->designation_id == 1){
        $projects = Projects::lists('projects', 'id');

        return View('addUser', compact('id', 'projects'));
        } else {
            return view('errors/503');
        }
    }
    /**
     * stores the newly added user
     *
     * @return homepage after adding user
     */
    public function save(Request $request)
    {
        $this->validate($request, [
        'name' => 'required|min:2',
        'email' => 'required|min:2',
    ]);

        $input = Input::only('name', 'email', 'project_id');
        $user = new Users($input);
        $user->save();

        Session::flash('message', 'Member was added');

        return Redirect::to('home');
    }
   /**
     * The number of users available for particular skill
     *
     * @return Users
     */
    public function availbleUsers($skill_id)
    {
        $users = DB::table('user_skills')
               ->select('users.id', 'users.name', 'users.email', 'projects.projects', 'skills.skills', 'users.project_id')
               ->leftJoin('users', 'user_skills.user_id', '=', 'users.id')
               ->leftJoin('projects', 'users.project_id', '=', 'projects.id')
               ->leftJoin('skills', 'user_skills.skill_id', '=', 'skills.id')
               ->where('user_skills.skill_id', '=', $skill_id)
               ->get();

        return $users;
    }
    /**
     * Displays the edit user page.
     *
     * @return editUserPage 
     */
    public function editUser(Request $request, $id)
    {   
        if(Auth::user()->designation_id == 1 && $request->isMethod('PATCH') && isset($id)) {
            $this->updateUser($id);
            
            return Redirect::to('home');
        } elseif($request->isMethod('GET') && isset($id)) {
            
            return view('addUser')->with(['user' => Users::findOrFail($id), 'projects' => Projects::lists('projects', 'id')]);
        }
    }
    /**
     * Updates the user details
     *
     * @return updatedUser
     */
    public function updateUser($id)
    {
        $user = Users::findOrFail($id);
        $input = Input::only('project_id');
        $user->where('id', $id)->update($input);
        Session::flash('message', 'User Details were updated');
    }
    /**
     * Deletes the user
     *
     * @return homepage with delete message
     */
    public function destroy($id)
    {
        if(Auth::user()->designation_id == 1){
        $user = Users::findOrFail($id);
        $user->delete();

        Session::flash('message', 'User was deleted Successfully');

        return Redirect::to('home');
        } else {
            return view('/error/503');
        }
    }
    /**
     * Displays new adding skills page
     *
     * @return updateSkills
     */
    public function registerSkills()
    {
        if(Auth::user()->designation_id == 1){
            
            return view('updateSkills');
        } else {
            return view('error/503');
        }
    }
    /**
     * saves the new added skills 
     *
     * @return homege with added skill message
     */
    public function saveSkills()
    {
        $input = Input::only('skills');
        $val = $input['skills'];
        $i = 0;
        for ($i = 0; $i < count($val); ++$i) {
            if (strlen($val[$i]) != 0) {
                DB::table('skills')->insert(array('skills' => $val[$i]));
            }
        }
        Session::flash('message', 'Skills was added');

        return Redirect::to('home');
    }
    /**
     * to get skills of the particular user
     *
     * @return skills
     */
    public function getIndividualSkills($user_id)
    {
        $skills = DB::table('user_skills')
               ->select('users.id', 'users.name', 'users.email', 'projects.projects', 'skills.skills', 'users.project_id')
               ->leftJoin('users', 'user_skills.user_id', '=', 'users.id')
               ->leftJoin('projects', 'users.project_id', '=', 'projects.id')
               ->leftJoin('skills', 'user_skills.skill_id', '=', 'skills.id')
               ->where('user_skills.user_id', '=', $user_id)
               ->get();

        return $skills;
    }
    public function removeIndividualSkill($user_id,$skill_id)
    { echo "delte";
        if(isset($user_id) && isset($skill_id)){
            DB::table('user_skills')
                    ->where('user_id', '=', $user_id)
                    ->where('skill_id','=', $skill_id)
                    ->delete();
        } 
    }
}
