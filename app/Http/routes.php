<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'WelcomeController@index');
Route::get('/login', 'ProfileController@login');
Route::get('/addUser', 'ProfileController@register');
Route::get('/addSkills/{id}','ProfileController@addSkills');
Route::post('/update/{id}','ProfileController@addSkills');
Route::post('save','ProfileController@save');
Route::get('/detail/{skill_id}','ProfileController@availbleUsers');
Route::get('/edit/user/{id}','ProfileController@editUser');
Route::patch('/updateUser/{id}','ProfileController@editUser');
Route::get('/deleteUser/{id}','ProfileController@destroy');
Route::get('/updateSkills','ProfileController@registerSkills');
Route::post('/updateSkills','ProfileController@saveSkills');
Route::get('/getUserSkills/{id}', 'ProfileController@getIndividualSkills');
Route::get('/deleteSkill/{uid}/{sid}', 'ProfileController@removeIndividualSkill');
Route::auth();

Route::get('/home', 'ProfileController@index');
Route::get('/logout', [
    'as'   => 'auth.logout',
    'uses' => 'Auth\AuthController@getLogout'
]);

Route::get('/post/users', ['middleware' => 'user.role', function () {
    return "Only big boys/girls can see this.";
}]);