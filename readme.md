## Profile Management System

##Basic Setup For PMS

1. Clone the repository in the local directory.
2. Run 'composer install'  command from the directory.
3. Run 'php artisan serve' to run the application in the localhost.

### License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)
